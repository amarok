
add_subdirectory( icons )

########### install files ###############

install(
        FILES
        album_cover_1.jpg
        album_cover_2.jpg
        album_cover_3.jpg
        album_cover_4.jpg
        album_cover_5.jpg
        album_cover_6.jpg
        album_cover_7.jpg
        album_cover_8.jpg
        album_cover_9.jpg
        album_cover_10.jpg
        amarok_icon.svg
        back_stars_grey.png
        currenttrack_play.png
        currenttrack_pause.png
        currenttrack_stop.png
        currenttrack_stop_small.png
        currenttrack_repeat.png
        currenttrack_repeat_small.png
        loading1.png
        loading2.png
        menu_sidepixmap.png
        more_albums.png
        musicbrainz.png
        nocover.png
        shadow_albumcover.png
        sbinner_stars.png
        smallstar.png
        icons/svg/sources.svg
        service-browser-element.svg
        sidebar_button.svg
        splash_screen.jpg
        star.png
        volumeslider-gradient.png
        volumeslider-handle.png
        volumeslider-handle_glow.png
        volumeslider-inset.png
        xine_logo.png
        toolbar-background.svg
        playlist_items.svg
        DESTINATION ${DATA_INSTALL_DIR}/amarok/images
)
