//
// C++ Interface: deviceconfiguredialog.h
//
// Description:
//
//
// Author: Jeff Mitchell <kde-dev@emailgoeshere.com>, (C) 2006
//         Martin Aumueller <aumuell@reserv.at>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DEVICECONFIGUREDIALOG_H
#define DEVICECONFIGUREDIALOG_H

#include "hintlineedit.h"
#include "MediaDevice.h"

#include <kdialog.h>

#include <QCheckBox>
#include <QRadioButton>


class Medium;

/**
    @author Jeff Mitchell <kde-dev@emailgoeshere.com>
*/
class DeviceConfigureDialog : public KDialog
{
    Q_OBJECT

    public:
        DeviceConfigureDialog( MediaDevice *device );
        ~DeviceConfigureDialog();
        bool successful() { return m_accepted; }

    private slots:
        void slotButtonClicked( KDialog::ButtonCode button );

    private:
        bool            m_accepted;
        MediaDevice     *m_device;

        HintLineEdit    *m_connectEdit;
        HintLineEdit    *m_disconnectEdit;
        QCheckBox       *m_transcodeCheck;
        QRadioButton    *m_transcodeAlways;
        QRadioButton    *m_transcodeWhenNecessary;
        QCheckBox       *m_transcodeRemove;

};

#endif
