project(context-currenttrack)

set(wiki_SRCS
WikipediaApplet.cpp)

include_directories( ../..
                    ../../..
                    ${KDE4_INCLUDE_DIR}/amarok ) # this way we don't need to prefix it with amarok/ (and it compiles this way too :)

kde4_add_plugin(amarok_context_applet_wikipedia ${wiki_SRCS})
target_link_libraries(amarok_context_applet_wikipedia amaroklib ${KDE4_KIO_LIBS})

install(TARGETS amarok_context_applet_wikipedia DESTINATION ${PLUGIN_INSTALL_DIR})
install(FILES amarok-context-applet-wikipedia.desktop DESTINATION ${SERVICES_INSTALL_DIR})
install(FILES amarok-wikipedia.svg amarok-wikipediaheader.svg DESTINATION ${DATA_INSTALL_DIR}/desktoptheme/default/widgets/ )
